#!/bin/bash

# P4D_MissionAnalyser.sh
# Ce script analyse le contenu des fichiers JSON créés par Pix4Dcapture.
# Licence GPLv3
# https://www.gnu.org/licenses/gpl-3.0.fr.html
# 2023/02/24
# Jean-Charles Braun

if [ $# -eq 0 ]; then
    echo "Please insert FILENAME + EPSG..."
    exit 0
fi

if [ ! -f "$1" ]; then
    echo "Please insert valid arguments..."
    exit 0
fi

ACTUAL=$(jq 'has("actual")' "$1")
FILENAME=$(basename "$1")
PATHNAME=$(dirname "$1")
cd "$PATHNAME"

if [ $ACTUAL = "true" ] && [ $# -eq 2 ]; then
    codeEPSG=$2
elif [ $ACTUAL = "true" ] && [ $# -eq 1 ]; then
    codeEPSG=3857
elif [ $ACTUAL = "false" ] && [ $# -eq 2 ]; then
    codeEPSG=$2
else
    echo "Please execute P4D FlightPlan..."
    exit 0
fi

jq -r '.flightPlan.waypoints[] | .location | @csv' "$FILENAME" > wayPoints.csv
jq -r '.actual.photos[] | [.pose.location[0,1], .mediaDescriptor.mediaId] | @csv' "$FILENAME" > uavPhotos.csv

list_x_WP=$(awk -F',' '{print $1}' wayPoints.csv)
list_y_WP=$(awk -F',' '{print $2}' wayPoints.csv)

list_x_IM=$(awk -F',' '{print $1}' uavPhotos.csv)
list_y_IM=$(awk -F',' '{print $2}' uavPhotos.csv)

sorted_x=$(awk -F',' '{print $1}' wayPoints.csv | sort)
sorted_y=$(awk -F',' '{print $2}' wayPoints.csv | sort)

x_min=$(echo "$sorted_x" | awk 'NR==1 {print}')
x_max=$(echo "$sorted_x" | awk 'END {print}')
y_min=$(echo "$sorted_y" | awk 'NR==1 {print}')
y_max=$(echo "$sorted_y" | awk 'END {print}')

coord_MAP=$(cat wayPoints.csv | tr ',' ' ' | gdaltransform -s_srs EPSG:4326 -t_srs EPSG:"$codeEPSG" | awk '{print "WP"(NR), $0}')
x_min_MAP=$(echo "$coord_MAP" | cut -d' ' -f2 | sort -n -r | tail -n1)
x_max_MAP=$(echo "$coord_MAP" | cut -d' ' -f2 | sort -n -r | head -n1)
y_min_MAP=$(echo "$coord_MAP" | cut -d' ' -f3 | sort -n -r | tail -n1)
y_max_MAP=$(echo "$coord_MAP" | cut -d' ' -f3 | sort -n -r | head -n1)

diff_x=$(echo "$x_max_MAP - $x_min_MAP" | bc -l)
diff_y=$(echo "$y_max_MAP - $y_min_MAP" | bc -l)
diffXY=$(echo "$diff_x / $diff_y" | bc -l)

echo "$coord_MAP" | awk '{k[NR]=$1; a[NR]=$2; b[NR]=$3} END {for(i=1; i<NR; i++) print k[i]"-"k[i+1], sqrt((a[i+1]-a[i])^2+(b[i+1]-b[i])^2)}' > sumDistWP.csv
awk '{sum+=$2} ENDFILE {print "SUM_TOTAL", sum >> FILENAME}' sumDistWP.csv

if (( $(echo "$diffXY > 1" | bc) )); then
    ajustX=1
    ajustY=$(echo "$diff_y / $diff_x" | bc -l)
    deltaX=0
    deltaY=$(echo "(360 - ($ajustY * 360)) / 2" | bc -l)
else
    ajustX=$(echo "$diff_x / $diff_y" | bc -l)
    ajustY=1
    deltaX=$(echo "(360 - ($ajustX * 360)) / 2" | bc -l)
    deltaY=0
fi

nbr_Lines=$(cat wayPoints.csv | wc -l)
for (( item=1; item<=nbr_Lines; item++ )); do
    X_value=$(echo "$list_x_WP" | awk -v nb="$item" 'NR==nb {print}')
    Y_value=$(echo "$list_y_WP" | awk -v nb="$item" 'NR==nb {print}')
    new_X_value=$(echo "((($X_value - $x_min) / ($x_max - $x_min)) * 360 * $ajustX) + $deltaX" | bc -l)
    new_Y_value=$(echo "((($Y_value - $y_min) / ($y_max - $y_min)) * 360 * $ajustY) + $deltaY" | bc -l)
    new_XY_WP+=("$new_X_value","$new_Y_value")
done
dataWP=$(echo ${new_XY_WP[@]})

nbr_Lines=$(cat uavPhotos.csv | wc -l)
for (( item=1; item<=nbr_Lines; item++ )); do
    X_value=$(echo "$list_x_IM" | awk -v nb="$item" 'NR==nb {print}')
    Y_value=$(echo "$list_y_IM" | awk -v nb="$item" 'NR==nb {print}')
    new_X_value=$(echo "((($X_value - $x_min) / ($x_max - $x_min)) * 360 * $ajustX) + $deltaX" | bc -l)
    new_Y_value=$(echo "((($Y_value - $y_min) / ($y_max - $y_min)) * 360 * $ajustY) + $deltaY" | bc -l)
    new_XY_IM+=("$new_X_value","$new_Y_value")
done
dataIM=$(echo ${new_XY_IM[@]})

circles=$(for point in $dataWP; do
              x=$(echo "$point" | cut -d',' -f1)
              y=$(echo "$point" | cut -d',' -f2)
              echo "circle $x,$y $x,$(echo $y + 5 | bc)"
          done)

triangles=$(for point in $dataIM; do
                x=$(echo "$point" | cut -d',' -f1)
                y=$(echo "$point" | cut -d',' -f2)
                echo "M $x,$(echo $y + 5 | bc) l +4,-7 -8,0 +4,+7"
            done)

function convertSecs {
    h=$(echo "$1 / 3600" | bc)
    m=$(echo "$1 % 3600 / 60" | bc)
    s=$(echo "$1 % 60" | bc)
    printf "%02dm:%02ds" $m $s
}

fplanID=$(jq -r '.metaData.name' "$FILENAME")
modelID=$(jq -r '.captureDevice.camera.name' "$FILENAME")
dateINI=$(date -u -d $(jq -r '.metaData.createdAt' "$FILENAME") "+%F @ %T")
dateEXE=$(date -u -d $(jq -r '.metaData.executedAt' "$FILENAME") "+%F @ %T")
initTimeSEC=$(jq -r '.expected.flightTime' "$FILENAME" | awk '{printf "%.0f", $1}')
initTimeHMS=$(convertSecs "$initTimeSEC")
execTimeSEC=$(jq -r '.actual.flightTime' "$FILENAME" | awk '{printf "%.0f", $1}')
execTimeHMS=$(convertSecs "$execTimeSEC")
initPhotos=$(jq -r '.expected.photoCount' "$FILENAME")
execPhotos=$(cat uavPhotos.csv | wc -l)
altiUAV=$(jq -r '.missionPlan.altitude' "$FILENAME")
tiltCAM=$(jq -r '.missionPlan.cameraPitch' "$FILENAME")
distSUM=$(awk 'END {printf "%.0f", $2}' sumDistWP.csv)
wptsNUM=$(cat wayPoints.csv | wc -l)
survTimeSEC=$(jq -r '.actual.photos[] | .pose.time' "$FILENAME" | awk 'NR==1 {i=$1} END {print $1-i}')
survTimeHMS=$(convertSecs "$survTimeSEC")
calcMpS=$(echo "$distSUM" "$survTimeSEC" | awk '{printf "%.6f", $1/$2}')
sensVAL=$(jq -r '.captureDevice.camera.parameters | [.focalLength, .imageWidth, .imageHeight, .sensorWidth] | @csv' "$FILENAME")
survGSD=$(echo "$sensVAL" | awk -v alti="$altiUAV" -F',' '{printf "%.3f", (alti * $4) / ($1 * $2) * 1000}')
overLap=$(jq -r '.missionPlan | .frontOverlap, .sideOverlap' "$FILENAME" | awk '{printf "~%.0f%%", $1*100}')

magick -size 640x360 canvas:MediumSeaGreen \
       -draw "fill white stroke black polyline $dataWP" \
       -draw "fill coral stroke red $circles" \
       -draw "fill none stroke red path 'M $dataIM'" \
       -draw "fill skyblue stroke blue path '$triangles'" \
       -draw "fill coral line 360,0 360,360" \
       -draw "fill coral line 400,314 530,314" \
       -flip \
       -font "Liberation-Mono" \
       -annotate +400+040 "P4D_MissionAnalyser" \
       -annotate +400+080 "$fplanID ($modelID)" \
       -annotate +400+100 "¤ ini: $dateINI" \
       -annotate +400+120 "¤ exe: $dateEXE" \
       -annotate +400+150 "Flight Time | Photos Count" \
       -annotate +400+170 "¤ ini: $initTimeHMS ("$initTimeSEC"s) | $initPhotos" \
       -annotate +400+190 "¤ exe: $execTimeHMS ("$execTimeSEC"s) | $execPhotos" \
       -annotate +400+220 "Altitude: ~$altiUAV m | Tilt: -"$tiltCAM"°" \
       -annotate +400+240 "Flight Path: $distSUM m ($wptsNUM WP)" \
       -annotate +400+260 "Survey Time: $survTimeHMS ("$survTimeSEC"s)" \
       -annotate +400+280 "Average Speed: $calcMpS m/s" \
       -annotate +400+300 "Pixel Size (GSD): $survGSD mm" \
       -annotate +400+320 "Front/Side Overlap: $overLap" \
       screenshot.png
