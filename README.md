# P4D_MissionAnalyser.sh

Ce script analyse le contenu des fichiers JSON créés par Pix4Dcapture.

## Prérequis

  * awk
  * gdal
  * imagemagick
  * liberation-fonts

## Utilisation

L'exécution du script nécessite 2 arguments :

  * le fichier JSON, habituellement nommé **details.pix4dcapture-mission**
  * le code **EPSG** de la localité géographique dans laquelle se situe la mission

Le lancement s'effectue dans un terminal :

```
$ bash P4D_MissionAnalyser.sh /chemin/vers/details.pix4dcapture-mission <EPSG>
```

## Fonctionnement

Si aucun code EPSG n’est indiqué, c’est le code EPSG:3857 qui sera utilisé, moins fiable mais suffisant pour afficher un visuel de la mission.  
Nous obtenons alors 3 fichiers CSV + 1 image PNG :

  1. **wayPoints.csv**  
     -> les coordonnées des points de repères (longitude / latitude) + la hauteur de vol
  2. **uavPhotos.csv**  
     -> la liste des photos + leurs coordonnées (longitude / latitude)
  3. **sumDistWP.csv**  
     -> les distances calculées entre les WayPoints + la distance totale
  4. **screenshot.png**  
     -> le visuel final (640px*360px) qui intègre plusieurs données extraites du fichier JSON

## Licence

Le programme ci-joint est publié sous licence GPLv3.  
[www.gnu.org/licenses/gpl-3.0.fr.html](https://www.gnu.org/licenses/gpl-3.0.fr.html)
